# Python API for Everest

A Python wrapper around Everest REST API.

## Requirements

* Python 2.7 (Python 3 is not supported)
* [Requests](http://docs.python-requests.org/en/latest/user/install/)

## Installation

Not needed, just place everest.py in your project.

## Quick Start

1. Use provided CLI to obtain authentication token:

```
python everest.py get-token -u EVEREST_USER -l TOKEN_LABEL
```

1. See test.py for examples of API usage.